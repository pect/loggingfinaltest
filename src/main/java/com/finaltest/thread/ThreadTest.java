package com.finaltest.thread;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.finaltest.levels.LogLevelable;
import com.finaltest.levels.impl.LogLevels;

public class ThreadTest {
	private static final Logger logger = LogManager.getLogger();
	
	public void createThreads() {
		LogLevelable logLevelable = new LogLevels();
        logLevelable.createLevels();
		Runnable run1 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.trace("Test TRACE");
		};
		Runnable run2 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.debug("Test DEBUG");
		};
		Runnable run3 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.info("Test INFO");
		};
		Runnable run4 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.warn("Test WARN");
		};
		Runnable run5 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.error("Test ERROR");
		};
		Runnable run6 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.fatal("Test FATAL");
		};
		Runnable run7 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.log(Level.getLevel("MUSTFIX"),"Test MUSTFIX");
		};
		Runnable run8 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.log(Level.getLevel("DATABASE"),"Test DATABASE");
		};
		Runnable run9 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.log(Level.getLevel("FAILOVER"),"Test FAILOVER");
		};
		Runnable run10 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.log(Level.getLevel("FIXLATER"),"Test FIXLATER");
		};
		Runnable run11 = () -> {
			ThreadContext.put("name", "Thelma Perez");
			ThreadContext.put("userName", "Jill Valentine");
			logger.log(Level.getLevel("MYDEBUG"),"Test MYDEBUG");
		};
		new Thread(run1,"Thread1").start();
		new Thread(run2,"Thread2").start();
		new Thread(run3,"Thread3").start();
		new Thread(run4,"Thread4").start();
		new Thread(run5,"Thread5").start();
		new Thread(run6,"Thread6").start();
		new Thread(run7,"Thread7").start();
		new Thread(run8,"Thread8").start();
		new Thread(run9,"Thread9").start();
		new Thread(run10,"Thread10").start();
		new Thread(run11,"Thread11").start();
	}
}
