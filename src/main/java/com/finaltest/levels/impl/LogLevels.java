package com.finaltest.levels.impl;

import org.apache.logging.log4j.Level;

import com.finaltest.levels.LogLevelable;

public class LogLevels implements LogLevelable{

	@Override
	public void createLevels() {
		Level codeLevel1 = Level.forName("MUSTFIX", 1);
		Level codeLevel2 = Level.forName("DATABASE", 250);
		Level codeLevel3 = Level.forName("FAILOVER", 350);
		Level codeLevel4 = Level.forName("FIXLATER", 450);
		Level codeLevel5 = Level.forName("MYDEBUG", 550);
	}

	@Override
	public void dummy1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dummy2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dummy3() {
		// TODO Auto-generated method stub
		
	}

}
